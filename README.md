# PCB-Skeleton-EBS-UULM

This repository contains a basic framework for the circuit board layout in combination with the FPGA board for a circuit board stack.

## Projektübersicht
Dieses Projekt ist die Rohversion einer Platine zur Verwendung im Platinenstack (EBS UULM). Steckerpositionen sind hier genau eingezeichnet, damit die Platinen sauber ineinander gesteckt und miteinander betrieben werden können.

Dieses Projekt ist kompatibel mit KiCAD Version 5.99 und neueren. Bei Verwendung einer älteren Version muss die Platine gemäß der unten zu sehenden Maße nachgestellt werden. Footprints müssen ebenfalls selbst angefertigt werden. Der Ursprung der Footprints ist aus dem Datenblatt ersichtlich und meistens die Mitte des Bauteils. Einzige Ausnahme hierfür ist der dreipolige Stromversorgungsstecker von Würth.

Grund für die Inkompatibilität mit älteren KiCAD-Versionen ist eine komplette Änderung des Speicherformats, siehe hier: https://www.kicad.org/blog/2020/05/Development-Highlight-New-schematic-and-symbol-library-file-formats-are-now-the-default/

Sämtliche Maße für die Bauteilpositionen sind im Layer "User.1" enthalten. Dieser kann einfach ausgeblendet werden.

![](./PCB-Layout.png)


## Dieses Projekt verwenden
Dieses Projekt kann auf mehreren Arten verwendet werden. Entweder wird nur auf einem lokalen Rechner weiter daran gearbeitet, oder es wird ein neues GitLab Repository daraus erstellt (Fork). Letztere Methode hat den Vorteil, dass sämtliche Änderungen commited und somit auch auf den GitLab-Servern gespeichert werden können.

### Projekt auf einen lokalen Rechner klonen
Um das Projekt als zip herunterzuladen, muss nur auf den Download-Button gedrückt werden. Das war's!

![](./_description-images/Clone-Project.png)

### Projekt über das Terminal auf einen lokalen Rechner klonen
Das Projekt kann auch über das Terminal simpel geklont werden (Das hier ist wahrscheinlich nichts für Windoof-User =P). Dazu erstmal den Link zu diesem Projekt kopieren:

![](./_description-images/Clone-Project-terminal.png)

und anschließend in der Konsole in dem gewünschten Verzeichnis auf dem lokalen Rechner den Befehl

    git clone <kopierter_Link_hier>

ausführen. Dieser Weg ist u.A. dann nützlich, wenn das Projekt geforkt wird.

### Projekt forken
Wenn ein Konto für gitlab.com vorhanden ist, kann dieses Projekt auch geforkt werden. Dabei wird eine Kopie dieses Repositories erstellt, in welchem Änderungen vorgenommen werden können. Zunächst dafür den "Fork"-Button oben rechts klicken:

![](./_description-images/Fork-Project-1.png)

Dann öffnet sich ein Fenster wie im folgenden Bild dargestellt. Name etc können hier geändert werden. <b>Für eine BA / MA bietet es sich an, das Projekt erstmal auf Privat zu stellen. Das stellt sicher, dass nur der Ersteller Einsicht in das Repo hat. Es ist kein Zugriff von Dritten möglich.</b> Nach dem Forken kann das Projekt über das Terminal geklont werden (siehe oben).

![](./_description-images/Fork-Project-2.png)

## Verwendete Footprints
Alle Footprints sind im Ordner FPGA_Board_BA_FalkoSchmidt.pretty zu finden:

- C_0402.kicad_mod: für alle generischen 0402 Bauteile verwendbar
- C_0603.kicad_mod: für alle generischen 0603 Bauteile verwendbar
- Conn_02x04_SMD_Pads.kicad_mod: für einen 8 poligen SMD Pad Stecker, z.B. zum Programmieren (JTAG, UART,...)
- Conn_FX8_80Pin.kicad_mod: 80 Pin Stecker (!ACHTUNG! HIER WURDE AUF DEN STECKERTYP FX8C GEWECHSELT!)
- Conn_FX8C_80Pin_Header.kicad_mod und Conn_FX8C_80Pin_Receptable.kicad_mod: Die 80 Pin Stecker links und rechts. Header und Receptable
- Connector_MPC3_Wuerth.kicad_mod: Zusätzlicher Stromstecker für die oberste Platine des Stacks (FPGA-Platine)
- Ferrite_0402.kicad_mod und Ferrite_0603_1608Metric_Pad0.98x0.95mm_HandSolder.kicad_mod: Ähnlich zu den ersten beiden Footprints, nur für Ferrite
- KMMX-B10-SMT4SB30.kicad_mod: Micro USB 3 Buchse
- LMK61.kicad_mod: Ultra-Low Jitter Pin Selectable Oscillator für die DAC / ADC Referenzclock
- MOLEX_47346_USB2_B.kicad_mod: USB 2 Stecker
- R_0402.kicad_mod: Ähnlich zu C_0402.kicad_mod
- SAMTEC_UMPS-03-03.5-G-VT-SM-WT-K.kicad_mod und SAMTEC_UMPT-03-01.5-G-VT-SM-WT-K.kicad_mod: Bladestecker für Stromversorgung

## Welche Stecker kommen wo hin?
Stromversorgung:
- SAMTEC_UMPT-03-01.5-G-VT-SM-WT-K.kicad_mod auf <b>TOP</b>
- SAMTEC_UMPS-03-03.5-G-VT-SM-WT-K.kicad_mod auf <b>BOTTOM</b>

Daten- / Finepitch-Stecker:
- Conn_FX8C_80Pin_Receptable.kicad_mod auf <b>TOP</b>
- Conn_FX8C_80Pin_Header.kicad_mod auf <b>BOTTOM</b>




## Bei Problemen...
Für Probleme mit den Libraries kann hier den ersten Anweisungen gefolgt werden: [Libraries importieren](https://gitlab.com/F-Schmidt/minicpu/-/blob/master/README.md)

